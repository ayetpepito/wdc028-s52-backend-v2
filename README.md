# WDC028-52 Data Visualization of CSV Data from Multiple File Uploads

## To be shared with the students as boilerplate code

## Setup Instructions
1. Pull from repo.
2. Install dependencies via the terminal command:
> npm install
3. Create the following directories in the root of our project:
- /uploads/cars
- /uploads/managers
4. Serve API via terminal command:
> node index.js